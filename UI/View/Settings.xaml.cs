﻿using Advanced_Scoring.UI.Controller;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Advanced_Scoring.UI.View
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class Settings : UserControl
    {
        private SettingsController Controller { get; set; }

        public Settings()
        {
            InitializeComponent();
        }

        internal void SetController(SettingsController controller)
        {
            Controller = controller;
            DataContext = Controller.Model;
        }

        private void BtnApply_Click(object sender, RoutedEventArgs e)
        {
            Controller.SaveData();
            Controller.Model.Changed = false;
        }

        private void BtnRevert_Click(object sender, RoutedEventArgs e)
        {
            this.DataContext = Controller.LoadData();
        }
    }
}
