﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_Scoring.UI.Model
{
    internal class SettingsModel :INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private bool _changed;

        private bool _accLeaderBoardFix;

        private int _gapTrackCountbackCount;

        private int _targetPitlaneTime;

        public bool ACCLeaderBoardFix
        {
            get => _accLeaderBoardFix;
            set
            {
                _accLeaderBoardFix = value;
                OnPropertyChanged();
                Changed = true;
            }
        }

        public int GapTrackCountbackCount
        {
            get => _gapTrackCountbackCount;
            set
            {
                _gapTrackCountbackCount = value;
                if (_gapTrackCountbackCount < 1)
                    _gapTrackCountbackCount = 1;

                OnPropertyChanged();
                Changed = true;
            }
        }

        public int TargetPitlaneTime
        {
            get => _targetPitlaneTime;
            set
            {
                _targetPitlaneTime = value;
                if (_targetPitlaneTime < 0)
                    _targetPitlaneTime = 0;

                OnPropertyChanged();
                Changed = true;
            }
        }

        public bool Changed
        {
            get => _changed;
            set
            {
                _changed = value;
                OnPropertyChanged();
            }
        }

        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
