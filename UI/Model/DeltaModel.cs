﻿using Advanced_Scoring.Helper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_Scoring.UI.Model
{
    public class DeltaModel
    {
        public ObservableCollection<DataRow> Deltas { get; private set; }

        public DeltaModel()
        {
            Deltas = new ObservableCollection<DataRow>();
        }

        public class DataRow : INotifyPropertyChanged
        {
            public string CarName { get; set; }
            public string TrackName { get; set; }
            public string LapTime { get; set; }

            public event PropertyChangedEventHandler PropertyChanged;

            public DataRow() { }

            public DataRow(string carName, string trackName, TimeSpan timeSpan)
            {
                CarName = carName;
                TrackName = trackName;
                SetLapTime(timeSpan);
            }

            

            public void SetLapTime(TimeSpan timeSpan)
            {
                LapTime = timeSpan.ToString(@"mm\:ss\.fff");
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("LapTime"));
            }
        }
    }
}
