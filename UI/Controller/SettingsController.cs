﻿using Advanced_Scoring.UI.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_Scoring.UI.Controller
{
    public class SettingsController
    {
        private AdvancedScoring AdvancedScoring { get; set; }
        internal SettingsModel Model { get; private set; }

        public SettingsController(AdvancedScoring advancedScoring)
        {
            AdvancedScoring = advancedScoring;

            LoadData();
        }

        internal SettingsModel LoadData()
        {
            Model = new SettingsModel()
            {
                ACCLeaderBoardFix = AdvancedScoring.Settings.ACCLeaderBoardFix,
                GapTrackCountbackCount = AdvancedScoring.Settings.GapTrackCountbackCount,
                Changed = false
            };

            return Model;
        }

        internal void SaveData()
        {
            AdvancedScoring.Settings.ACCLeaderBoardFix = Model.ACCLeaderBoardFix;
            AdvancedScoring.Settings.GapTrackCountbackCount = Model.GapTrackCountbackCount;
            AdvancedScoring.Settings.PitlaneTargetTime = Model.TargetPitlaneTime;

            //Some Moduls need to be informed of changes
            AdvancedScoring.Tracker.UpdateTrackerSetting();
        }
    }
}
