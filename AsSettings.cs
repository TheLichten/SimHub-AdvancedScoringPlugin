﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_Scoring
{
    public class AsSettings
    {
        public bool ACCLeaderBoardFix { get; set; } = false;

        public int GapTrackCountbackCount { get; set; } = 5;

        public double PitlaneTargetTime { get; set; } = 0;
    }
}
