﻿using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_Scoring.Helper
{
    public partial class ManagedOutputDelta<T, D> : ManagedDelta<T> where T : IComputeMix<T>
    {
        private AttachedProperty<D> DeltaProperty { get; set; }

        public ManagedOutputDelta(PluginManager pluginManager, string name, StorageType storageType, Type pluginType) : base(pluginManager, name, storageType)
        {
            DeltaProperty = new AttachedProperty<D>();
            pluginManager.AttachProperty(name, pluginType, DeltaProperty);
        }

        public override void Update(T Data, double lapFrac)
        {
            base.Update(Data, lapFrac);
            OutputDelta(Data, lapFrac);
        }

        protected void OutputDelta(T Data, double lapFrac)
        {
            //Updating the Delta Property, only if set up to do so
            if (Reference != null)
            {
                T refernce = Reference.GetValue(lapFrac);
                object delta = Data.Subtract(refernce).GetValue();

                D val = (D)delta; //We assume it is of the type D, otherwise there is a fundamental problem
                DeltaProperty.Value = val;
            }
            else
            {
                DeltaProperty.Value = default(D);
            }
        }
    }
}
