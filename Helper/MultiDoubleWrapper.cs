﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_Scoring.Helper
{
    public class MultiDoubleWrapper : IComputeMix<MultiDoubleWrapper>
    {
        public readonly double[] Value;

        public MultiDoubleWrapper(double[] value)
        {
            Value = value;
        }

        public MultiDoubleWrapper ProduceMixture(MultiDoubleWrapper other, double sliderToOther)
        {
            if (sliderToOther <= 0.0 || other == null || other.Value == null)
                return this;
            if (sliderToOther >= 1.0 || Value == null)
                return other;

            if (other.Value.Length != Value.Length) //Missmatched array length
                return this;

            double[] delta = new double[Value.Length];

            for (int i = 0; i < Value.Length; i++)
            {
                delta[i] = other.Value[i] - Value[i];
                delta[i] = (delta[i] * sliderToOther) + Value[i];
            }

            return new MultiDoubleWrapper(delta);
        }

        public MultiDoubleWrapper Minimum(MultiDoubleWrapper other)
        {
            if (other == null || other.Value == null)
                return this;
            if (Value == null)
                return other;
            if (other.Value.Length != Value.Length) //Missmatched array length
                return this;

            double[] delta = new double[Value.Length];

            for (int i = 0; i < Value.Length; i++)
            {
                if (Value[i] < other.Value[i])
                    delta[i] = Value[i];
                else
                    delta[i] = other.Value[i];
            }

            return new MultiDoubleWrapper(delta);
        }

        public MultiDoubleWrapper Maximum(MultiDoubleWrapper other)
        {
            if (other == null || other.Value == null)
                return this;
            if (Value == null)
                return other;
            if (other.Value.Length != Value.Length) //Missmatched array length
                return this;

            double[] delta = new double[Value.Length];

            for (int i = 0; i < Value.Length; i++)
            {
                if (Value[i] > other.Value[i])
                    delta[i] = Value[i];
                else
                    delta[i] = other.Value[i];
            }

            return new MultiDoubleWrapper(delta);
        }

        public object GetValue()
        {
            return Value;
        }

        public MultiDoubleWrapper Subtract(MultiDoubleWrapper subtrahend)
        {
            if (subtrahend == null || subtrahend.Value == null)
                return this;
            if (Value == null)
                return subtrahend;
            if (subtrahend.Value.Length != Value.Length) //Missmatched array length
                return this;

            double[] result = new double[Value.Length];

            for (int i = 0; i < Value.Length; i++)
            {
                result[i] = Value[i] - subtrahend.Value[i];
            }

            return new MultiDoubleWrapper(result);
        }
    }
}
