﻿using GameReaderCommon;
using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advanced_Scoring
{
    internal class AdvancedFuel
    {
        private AdvancedScoring main;

        private const string PRE = "Fuel_";
        private const string PITIN = "PitInLap";

        internal AdvancedFuel(AdvancedScoring main)
        {
            this.main = main;
        }

        internal void Init(PluginManager pluginManager)
        {
            //pluginManager.AddProperty(PRE + PITIN, this.GetType(), Int32.MinValue.GetType(), "The Lap you need to come in!");
        }

        internal void DataUpdate(PluginManager pluginManager, ref GameData data)
        {

        }
    }
}
