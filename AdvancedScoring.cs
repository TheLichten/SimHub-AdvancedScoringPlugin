﻿using GameReaderCommon;
using SimHub.Plugins;
using System;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Collections.Generic;
using Lichten.SimHubHelper;
using Advanced_Scoring.UI;
using Advanced_Scoring.UI.Controller;
using System.Windows.Media;
using Advanced_Scoring.Helper;

namespace Advanced_Scoring
{

    [PluginName("Advanced Scoring")]
    [PluginAuthor("Lukas Lichten")]
    [PluginDescription("Adds a range of additional calculated info")]
    public class AdvancedScoring : IPlugin, IDataPlugin, IWPFSettingsV2
    {
        public const string POSINCLASS = "PositionInClass";
        public const string OPPONENTS_INCLASS = "OpponentsInClass";
        public const string OPPONENTS = "Opponents";

        public const string LAP_PERCENT = "LapPercent";

        public const string LEADER = "Leader";
        public const string CLASS_LEADER = "ClassLeader";

        public const string NEXTPREFIX = "DriverNext";
        public const string BEHINDPREFIX = "DriverBehind";
        public const string GAPMIDFIX = "_Gap";
        public const string ACTIVEGAP = "_ActiveGap";

        public const string DELTASUFIX = "DeltaPreviousLap";
        public const string DELTA_AVGSUFIX = "DeltaPerLapAvg";

        public const string TOTAL_RACE_LENGTH = "TotalRaceTime";
        public const string PITSTOP_WINDOW = "PitWindow";
        public const string OPENS = "Opens";
        public const string CLOSES = "Closes";
        public const string STINT = "StintTimeRemaining";
        public const string DRIVE_TIME = "TotalDriveTimeRemaining";

        public const string ACC = "ACC_";

        private readonly long STINT_TOTAL_TIME_OFF = (long)ushort.MaxValue * 1000;


        private AdvancedFuel fuelTracker;
        private List<IDataEvent> dataEvents;

        internal DeltaManager DeltaManager { get; private set; }
        internal PitlaneDelta PitlaneDelta { get; private set; }
        internal Tracker Tracker { get; private set; }
        public AsSettings Settings { get; set; }
        public MasterController MasterUIController { get; private set; }

        /// <summary>
        /// Instance of the current plugin manager
        /// </summary>
        public PluginManager PluginManager { get; set; }

        public ImageSource PictureIcon => this.ToIcon(Properties.Resources.icon);

        public string LeftMenuTitle => "Advanced Scoring";

        /// <summary>
        /// Called after plugins startup
        /// </summary>
        /// <param name="pluginManager"></param>
        public void Init(PluginManager pluginManager)
        {
            PluginManager = pluginManager;

            // Load settings
            Settings = this.ReadCommonSettings<AsSettings>("AdvancedScoring", () => new AsSettings());


            Tracker = new Tracker(this);
            pluginManager.NewLap += new PluginManager.NewLapDelegate(NewLapUpdate);

            fuelTracker = new AdvancedFuel(this);
            fuelTracker.Init(pluginManager);

            DeltaManager = new DeltaManager(pluginManager, this.GetType());

            PitlaneDelta = new PitlaneDelta(this);

            pluginManager.AddProperty(LAP_PERCENT, this.GetType(), Double.MinValue.GetType(), "Fraction (0-1) of the Lap completed, slight under and overflows to be expected");

            pluginManager.AddProperty(NEXTPREFIX + ACTIVEGAP, this.GetType(), "".GetType(), "The gap to the car infront");
            pluginManager.AddProperty(BEHINDPREFIX + ACTIVEGAP, this.GetType(), "".GetType(), "The gap to the car behind");

            pluginManager.AddProperty(LEADER + ACTIVEGAP, this.GetType(), "".GetType(), "The gap to the overall leader");
            pluginManager.AddProperty(CLASS_LEADER + ACTIVEGAP, this.GetType(), "".GetType(), "The gap to the class leader");

            for (int i = 0; i < Settings.GapTrackCountbackCount; i++)
            {
                pluginManager.AddProperty(NEXTPREFIX + GAPMIDFIX + DELTASUFIX + i, this.GetType(), Double.MinValue.GetType(), "The delta of the gap to the car infront from the start of that lap to it's end");
                pluginManager.AddProperty(BEHINDPREFIX + GAPMIDFIX + DELTASUFIX + i, this.GetType(), Double.MinValue.GetType(), "The delta of the gap to the car behind from the start of that lap to it's end");
            }

            pluginManager.AddProperty(NEXTPREFIX + GAPMIDFIX + DELTA_AVGSUFIX, this.GetType(), Double.MinValue.GetType(), "The average delta of the gap to the car infront over the last " + Settings.GapTrackCountbackCount + " laps");
            pluginManager.AddProperty(BEHINDPREFIX + GAPMIDFIX + DELTA_AVGSUFIX, this.GetType(), Double.MinValue.GetType(), "The average delta of the gap to the car behind over the last " + Settings.GapTrackCountbackCount + " laps");

            pluginManager.AddProperty(POSINCLASS, this.GetType(), Int32.MinValue.GetType(), "The Players Position in his class");
            pluginManager.AddProperty(OPPONENTS_INCLASS, this.GetType(), Int32.MinValue.GetType(), "Cars in the same class as the player");
            pluginManager.AddProperty(OPPONENTS, this.GetType(), Int32.MinValue.GetType(), "Cars in race");

            pluginManager.AddProperty(ACC + TOTAL_RACE_LENGTH, this.GetType(), TimeSpan.Zero.GetType(), "ACC only, total race time, including what is to come and what has been completed");
            pluginManager.AddProperty(ACC + PITSTOP_WINDOW + OPENS, this.GetType(), TimeSpan.Zero.GetType(), "ACC only, time till the pitwindow opens");
            pluginManager.AddProperty(ACC + PITSTOP_WINDOW + CLOSES, this.GetType(), TimeSpan.Zero.GetType(), "ACC only, time till the pitwindow closes");
            pluginManager.AddProperty(ACC + "In" + PITSTOP_WINDOW, this.GetType(), false.GetType(), "ACC only, if the pitwindow is currently open");
            pluginManager.AddProperty(ACC + STINT, this.GetType(), TimeSpan.Zero.GetType(), "ACC only, remaining stint time till a pitstop has to be performed");
            pluginManager.AddProperty(ACC + DRIVE_TIME, this.GetType(), TimeSpan.Zero.GetType(), "ACC only, remaining time in the race for this driver");

            pluginManager.AddProperty("A_Time", this.GetType(), double.MaxValue.GetType());

            dataEvents = new List<IDataEvent>();
            dataEvents.Add(new DataOnOffEventHelper(ACC + PITSTOP_WINDOW, this.GetType(), "AdvancedScoring.ACC_InPitWindow", pluginManager, "Opened", "Closed"));
        }

        /// <summary>
        /// called one time per game data update
        /// </summary>
        /// <param name="pluginManager"></param>
        /// <param name="data"></param>
        public void DataUpdate(PluginManager pluginManager, ref GameData data)
        {
            System.Diagnostics.Stopwatch stopwatch = new System.Diagnostics.Stopwatch();
            stopwatch.Start();

            if (data.GameRunning && data.NewData != null)
            {
                PitlaneDelta.Update(pluginManager, data); //Timing Sensitive, we execute it first

                //ACC Specific
                TimeSpan totalRaceLength = TimeSpan.Zero;
                TimeSpan pitWindowOpen = TimeSpan.Zero;
                TimeSpan pitWindowClose = TimeSpan.Zero;
                bool pitWindowStatus = false;
                TimeSpan stintTimeRemaining = TimeSpan.Zero;
                TimeSpan driveTimeRemaining = TimeSpan.Zero;

                if (data.GameName == "AssettoCorsaCompetizione")
                {
                    // ACC experiment to remove disconnected drivers
                    if (Settings.ACCLeaderBoardFix && data.NewData.Opponents != null)
                    {
                        object spec = pluginManager.GetPropertyValue("DataCorePlugin.GameData.Spectating");

                        if (!(spec is bool isSpectating && isSpectating))
                            CorrectAccLeaderboard(data.NewData.Opponents);
                    }

                    object timeTemp = pluginManager.GetPropertyValue("DataCorePlugin.GameRawData.Realtime.SessionTime");
                    TimeSpan timeElapsed = timeTemp != null ? (TimeSpan)timeTemp : TimeSpan.Zero;

                    timeTemp = pluginManager.GetPropertyValue("DataCorePlugin.GameRawData.Realtime.SessionEndTime");
                    totalRaceLength = timeTemp != null ? (TimeSpan)timeTemp : TimeSpan.Zero;
                    totalRaceLength = totalRaceLength.Add(timeElapsed);
                    totalRaceLength = new TimeSpan(((long)Math.Round(totalRaceLength.Ticks / 10000000.0)) * 10000000); //Rounding

                    long pitstart = (int)pluginManager.GetPropertyValue("DataCorePlugin.GameRawData.StaticInfo.PitWindowStart");
                    long pitend = (int)pluginManager.GetPropertyValue("DataCorePlugin.GameRawData.StaticInfo.PitWindowEnd");

                    if (pitstart < pitend) //If pitend is smaller then pitstart then there is no pitwindow
                    {
                        pitWindowOpen = new TimeSpan(pitstart * 10000);
                        pitWindowClose = new TimeSpan(pitend * 10000);

                        // Fixes problem in multiplayer where for some reason we get negative numbers for the pitWindowOpen, so this should fix it
                        if (pitWindowOpen < TimeSpan.Zero)
                        {
                            TimeSpan pitWindowLength = pitWindowClose - pitWindowOpen;
                            pitWindowOpen = new TimeSpan((totalRaceLength - pitWindowLength).Ticks / 2);
                            pitWindowClose = pitWindowOpen + pitWindowLength;
                        }

                        //I have seen odd behavior with these values, but I trust them for now to actually be correct
                        pitWindowOpen = pitWindowOpen.Subtract(timeElapsed);
                        pitWindowClose = pitWindowClose.Subtract(timeElapsed);

                        if (pitWindowOpen.TotalMilliseconds < 0)
                        {
                            pitWindowOpen = TimeSpan.Zero;
                            if (pitWindowClose.TotalMilliseconds < 0)
                                pitWindowClose = TimeSpan.Zero;
                            else
                                pitWindowStatus = true;
                        }

                    }

                    long driverStintTime = (int)pluginManager.GetPropertyValue("DataCorePlugin.GameRawData.Graphics.DriverStintTimeLeft");
                    long driverStintTotalTime = (int)pluginManager.GetPropertyValue("DataCorePlugin.GameRawData.Graphics.DriverStintTotalTimeLeft");

                    if (driverStintTime > 0)
                        stintTimeRemaining = new TimeSpan(driverStintTime * 10000);
                    if (driverStintTotalTime > 0 && driverStintTotalTime != STINT_TOTAL_TIME_OFF)
                        driveTimeRemaining = new TimeSpan(driverStintTotalTime * 10000);


                }

                pluginManager.SetPropertyValue(ACC + TOTAL_RACE_LENGTH, this.GetType(), totalRaceLength);
                pluginManager.SetPropertyValue(ACC + PITSTOP_WINDOW + OPENS, this.GetType(), pitWindowOpen);
                pluginManager.SetPropertyValue(ACC + PITSTOP_WINDOW + CLOSES, this.GetType(), pitWindowClose);
                pluginManager.SetPropertyValue(ACC + "In" + PITSTOP_WINDOW, this.GetType(), pitWindowStatus);
                pluginManager.SetPropertyValue(ACC + STINT, this.GetType(), stintTimeRemaining);
                pluginManager.SetPropertyValue(ACC + DRIVE_TIME, this.GetType(), driveTimeRemaining);

                //Gaps:
                Opponent player = data.NewData.OpponentAtPosition(1, true, false, false);

                Opponent next = data.NewData.OpponentAtPosition(0, true, false, false);
                Opponent behind = data.NewData.OpponentAtPosition(2, true, false, false);

                pluginManager.SetPropertyValue(NEXTPREFIX + ACTIVEGAP, this.GetType(), Tracker.GetPrintableGap(player, next, data));
                pluginManager.SetPropertyValue(BEHINDPREFIX + ACTIVEGAP, this.GetType(), Tracker.GetPrintableGap(player, behind, data));


                pluginManager.SetPropertyValue(LEADER + ACTIVEGAP, this.GetType(), Tracker.GetPrintableGap(player, data.NewData.OpponentAtPosition(1, false, false, false), data));
                pluginManager.SetPropertyValue(CLASS_LEADER + ACTIVEGAP, this.GetType(), Tracker.GetPrintableGap(player, data.NewData.OpponentAtPosition(1, false, false, true), data));

                if (player != null)
                {
                    pluginManager.SetPropertyValue(POSINCLASS, this.GetType(), player.PositionInClass);
                    pluginManager.SetPropertyValue(OPPONENTS_INCLASS, this.GetType(), data.NewData.OpponentsPlayerClass.Count);
                    pluginManager.SetPropertyValue(LAP_PERCENT, this.GetType(), data.NewData.TrackPositionPercent);
                }
                else
                {
                    pluginManager.SetPropertyValue(POSINCLASS, this.GetType(), -1);
                    pluginManager.SetPropertyValue(OPPONENTS_INCLASS, this.GetType(), -1);
                    pluginManager.SetPropertyValue(LAP_PERCENT, this.GetType(), 0);
                }

                pluginManager.SetPropertyValue(OPPONENTS, this.GetType(), data.NewData.Opponents != null ? data.NewData.Opponents.Count : 0);



                //DeltaManager
                DeltaManager.Update(pluginManager, data);

                //Events
                foreach (var item in dataEvents)
                {
                    item.Update(pluginManager);
                }
            }

            stopwatch.Stop();
            pluginManager.SetPropertyValue("A_Time", this.GetType(), stopwatch.Elapsed.TotalMilliseconds);

        }



        /// <summary>
        /// Updates the tracker
        /// </summary>
        /// <param name="completedLapNumber"></param>
        /// <param name="testLap"></param>
        /// <param name="manager"></param>
        /// <param name="data"></param>
        public void NewLapUpdate(int completedLapNumber, bool testLap, PluginManager manager, ref GameData data)
        {
            if (data.GameRunning && data.NewData != null)
            {
                if (data.GameName == "AssettoCorsaCompetizione" && Settings.ACCLeaderBoardFix)
                {
                    object spec = manager.GetPropertyValue("DataCorePlugin.GameData.Spectating");

                    if (!(spec is bool isSpectating && isSpectating))
                        CorrectAccLeaderboard(data.NewData.Opponents);
                }

                if (data.NewData.CompletedLaps > 0)
                {
                    Tracker.NewLapUpdate(data.NewData.CompletedLaps, manager, data);
                    DeltaManager.NewLap(manager, data);
                }
                else
                {
                    Tracker.Wipe(manager, data);
                    PitlaneDelta.Wipe();
                    DeltaManager.Wipe();
                }

                
            }
        }

        /// <summary>
        /// Called at plugin manager stop, close/displose anything needed here !
        /// </summary>
        /// <param name="pluginManager"></param>
        public void End(PluginManager pluginManager)
        {
            DeltaManager.End();
            PitlaneDelta.End();

            this.SaveCommonSettings<AsSettings>("AdvancedScoring", Settings);
        }

        /// <summary>
        /// Call this methode only if the game is ACC (or another game this fix works for)
        /// This removes all cars that have no coordinates, which indicates that the car has disconnected
        /// </summary>
        /// <param name="listOfOpponents">data.NewData.Opponents</param>
        public static void CorrectAccLeaderboard(List<Opponent> listOfOpponents)
        {
            // ACC experiment to remove disconnected drivers
            for (int i = 0; i < listOfOpponents.Count; i++)
            {
                if (listOfOpponents[i].Coordinates == null)
                {
                    listOfOpponents.RemoveAt(i);
                    i--;
                }
            }
        }

        public System.Windows.Controls.Control GetWPFSettingsControl(PluginManager pluginManager)
        {
            MasterUIController = new MasterController(this);

            return MasterUIController.MainView;
        }


    }
}