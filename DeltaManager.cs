﻿using GameReaderCommon;
using SimHub.Plugins;
using System.IO;
using System;
using Newtonsoft.Json;
using Advanced_Scoring.Helper;

namespace Advanced_Scoring
{
    public class DeltaManager
    {
        public const string DELTA = "Delta";
        public const string OVERALL_BEST_LAP = "AllTimeBestLap";
        public const string TIRE_PRESSURE = "LastLapAverageTyrePressures";
        public const string PERSONAL = "Personal";
        public const string TARGET = "TargetLap";

        //Managed Deltas
        public ManagedDelta<DoubleWrapper> OverallBestLap { get; private set; }
        public ManagedDelta<MultiDoubleWrapper> TirePressures { get; private set; }
        public ManagedDelta<DoubleWrapper> TargetTimes { get; private set; }
        
        //Read Only Deltas
        public Delta<DoubleWrapper> StintBestLap { get; private set; }
        public Delta<DoubleWrapper> SessionBestLap { get; private set; }
        public Delta<DoubleWrapper> LastLap { get; private set; }

        //Attached Properties
        private AttachedProperty<double> StintBestDelta { get; set; }
        private AttachedProperty<double> SessionBestDelta { get; set; }
        private AttachedProperty<double> LastLapDelta { get; set; }
        private AttachedProperty<double>[] AverageTirePressure { get; set; }
        private AttachedProperty<TimeSpan> OverallBestLapTime { get; set; }
        private AttachedProperty<TimeSpan> TargetLapTime { get; set; }
        private AttachedProperty<TimeSpan> StintBestLapTime { get; set; }
        private AttachedProperty<TimeSpan> SessionBestLapTime { get; set; }
        private AttachedProperty<TimeSpan> LastLapTime { get; set; }


        //Fields dealing with the Update Cycle, allowing it to compensate for lapFrac's that don't properly update
        private double previousLapFrac = 0;
        private double distanceFromLastLapFrac = 0;
        private double[] previousCycleCoordinates;

        //other fields
        private string Session = "";

        public DeltaManager(PluginManager pluginManager, Type pluginType)
        {
            OverallBestLap = new ManagedOutputDelta<DoubleWrapper, double>(pluginManager, PERSONAL + OVERALL_BEST_LAP + DELTA, StorageType.Seperate, pluginType);
            OverallBestLapTime = new AttachedProperty<TimeSpan>();
            pluginManager.AttachProperty(PERSONAL + OVERALL_BEST_LAP, pluginType, OverallBestLapTime);

            TargetTimes = new ManagedOutputOnlyDelta<DoubleWrapper, double>(pluginManager, TARGET + DELTA, StorageType.Json, pluginType);
            TargetLapTime = new AttachedProperty<TimeSpan>();
            pluginManager.AttachProperty(TARGET, pluginType, TargetLapTime);

            StintBestDelta = new AttachedProperty<double>();
            StintBestLapTime = new AttachedProperty<TimeSpan>();
            pluginManager.AttachProperty(PERSONAL + "StintBestLap" + DELTA, pluginType, StintBestDelta);
            pluginManager.AttachProperty(PERSONAL + "StintBestLap", pluginType, StintBestLapTime);
            SessionBestDelta = new AttachedProperty<double>();
            SessionBestLapTime = new AttachedProperty<TimeSpan>();
            pluginManager.AttachProperty(PERSONAL + "SessionBestLap" + DELTA, pluginType, SessionBestDelta);
            pluginManager.AttachProperty(PERSONAL + "SessionBestLap", pluginType, SessionBestLapTime);
            LastLapDelta = new AttachedProperty<double>();
            LastLapTime = new AttachedProperty<TimeSpan>();
            pluginManager.AttachProperty(PERSONAL + "LastLap" + DELTA, pluginType, LastLapDelta);
            pluginManager.AttachProperty(PERSONAL + "LastLap", pluginType, LastLapTime);


            TirePressures = new ManagedDelta<MultiDoubleWrapper>(pluginManager, TIRE_PRESSURE, StorageType.Off);
            AverageTirePressure = new AttachedProperty<double>[] { new AttachedProperty<double>(), new AttachedProperty<double>(), new AttachedProperty<double>(), new AttachedProperty<double>() };
            pluginManager.AttachProperty(TIRE_PRESSURE + "FL", pluginType, AverageTirePressure[0]);
            pluginManager.AttachProperty(TIRE_PRESSURE + "FR", pluginType, AverageTirePressure[1]);
            pluginManager.AttachProperty(TIRE_PRESSURE + "RL", pluginType, AverageTirePressure[2]);
            pluginManager.AttachProperty(TIRE_PRESSURE + "RR", pluginType, AverageTirePressure[3]);
        }

        public void Update(PluginManager pluginManager, GameData data)
        {
            double lapFrac = data.NewData.TrackPositionPercent;

            //Correcting rf2 not updating trackposition at 60hz
            double[] coor = data.NewData.CarCoordinates;

            if (previousLapFrac == lapFrac && lapFrac != 0)
            {
                double triangle = Math.Pow(coor[0] - previousCycleCoordinates[0], 2) + Math.Pow(coor[1] - previousCycleCoordinates[1], 2) + Math.Pow(coor[2] - previousCycleCoordinates[2], 2); //x = a, y = b, z = c and r is wanted: a² + b² = d² and d² + c² = r²
                distanceFromLastLapFrac += Math.Sqrt(triangle);


                double addFrac = distanceFromLastLapFrac / data.NewData.TrackLength;

                lapFrac += addFrac;
            }
            else
            {
                distanceFromLastLapFrac = 0;
                previousLapFrac = lapFrac;
            }

            previousCycleCoordinates = coor;

            //Clearing read onlys if necessary
            if (data.NewData.IsInPitLane == 1)
                StintBestLap = null;

            if (data.NewData.SessionTypeName != null && data.NewData.SessionTypeName != Session)
            {
                SessionBestLap = null;
                StintBestLap = null;
                LastLap = null;
                Session = data.NewData.SessionTypeName;
            }

            //Deltas
            if (data.NewData.CurrentLapTime != null)
            {
                DoubleWrapper currentTime = new DoubleWrapper(data.NewData.CurrentLapTime.TotalSeconds);
                OverallBestLap.Update(currentTime, lapFrac);
                TargetTimes.Update(currentTime, lapFrac);

                StintBestDelta.RawValue = StintBestLap != null ? currentTime.Subtract(StintBestLap.GetValue(lapFrac)).GetValue() : 0.0;
                SessionBestDelta.RawValue = SessionBestLap != null ? currentTime.Subtract(SessionBestLap.GetValue(lapFrac)).GetValue() : 0.0;
                LastLapDelta.RawValue = LastLap != null ? currentTime.Subtract(LastLap.GetValue(lapFrac)).GetValue() : 0.0;
            }
                

            
            double[] tyrePressures = { data.NewData.TyrePressureFrontLeft, data.NewData.TyrePressureFrontRight, data.NewData.TyrePressureRearLeft, data.NewData.TyrePressureRearRight };
            TirePressures.Update(new MultiDoubleWrapper(tyrePressures), lapFrac);
            

            //Properties
            OverallBestLapTime.Value = OverallBestLap.Reference != null && OverallBestLap.Reference.Eval is double ? TimeSpan.FromSeconds((double)OverallBestLap.Reference.Eval) : TimeSpan.Zero;
            TargetLapTime.Value = TargetTimes.Reference != null && TargetTimes.Reference.Eval is double ? TimeSpan.FromSeconds((double)TargetTimes.Reference.Eval) : TimeSpan.Zero;
            StintBestLapTime.Value = StintBestLap != null ? TimeSpan.FromSeconds((double)StintBestLap.Eval) : TimeSpan.Zero;
            SessionBestLapTime.Value = SessionBestLap != null ? TimeSpan.FromSeconds((double)SessionBestLap.Eval) : TimeSpan.Zero;
            LastLapTime.Value = LastLap != null ? TimeSpan.FromSeconds((double)LastLap.Eval) : TimeSpan.Zero;

            double[] averagePressure = TirePressures.Reference != null ? TirePressures.Reference.AverageValue.Value : new double[4];
            for (int i = 0; i < AverageTirePressure.Length; i++)
            {
                AverageTirePressure[i].Value = averagePressure[i];
            }
        }

        public void NewLap(PluginManager pluginManager, GameData data)
        {
            double lastTime = data.NewData.LastLapTime.TotalSeconds;

            bool flush = false;
            if (lastTime != 0 && data.NewData.IsLapValid)
            {
                flush = true;
                if (OverallBestLap.Reference != null && OverallBestLap.Reference.Eval is double)
                {
                    double best = (double) OverallBestLap.Reference.Eval;
                    flush = lastTime < best || best == 0;
                }

                OverallBestLap.Cache.Eval = lastTime;
                TirePressures.Cache.Eval = lastTime;

                if (OverallBestLap.Cache.IsValidLap())
                {
                    LastLap = OverallBestLap.Cache;
                    if (StintBestLap == null || ((double)StintBestLap.Eval) > lastTime)
                    {
                        StintBestLap = OverallBestLap.Cache;
                    }
                    if (SessionBestLap == null || ((double)SessionBestLap.Eval) > lastTime)
                    {
                        SessionBestLap = OverallBestLap.Cache;
                    }
                }
            }

            OverallBestLap.NewLap(flush && OverallBestLap.Cache.IsValidLap());

            TirePressures.NewLap(TirePressures.Cache.IsValidLap());
        }

        /// <summary>
        /// Wipes out the temp data, so session best, last lap, etc.
        /// All time best is maintained, but their Cache is cleared (so the lap they were building, as the lap should not override the current best)
        /// </summary>
        public void Wipe()
        {
            LastLap = null;
            StintBestLap = null;
            SessionBestLap = null;


            OverallBestLap.NewLap(false);
            TirePressures.NewLap(false);
        }

        public void End()
        {
            OverallBestLap.WriteData();
            TargetTimes.WriteData();
        }
    }
}