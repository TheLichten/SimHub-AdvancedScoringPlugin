﻿using GameReaderCommon;
using SimHub.Plugins;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Advanced_Scoring.Helper;

namespace Advanced_Scoring
{
    internal class Tracker
    {
        private readonly AdvancedScoring Main;

        private int Position;
        private string CarInfrontName;
        private string CarBehindName;
        private string CarName;
        private string GameName;

        private int Lap;

        private LinkedQueue<double> InfrontDelta { get; set; }
        private LinkedQueue<double> BehindDelta { get; set; }

        private double InfrontGap = Double.NaN;
        private double BehindGap = Double.NaN;

        internal Tracker(AdvancedScoring main)
        {
            this.Main = main;
            this.Lap = 0;

            InfrontDelta = new LinkedQueue<double>(main.Settings.GapTrackCountbackCount);
            BehindDelta = new LinkedQueue<double>(main.Settings.GapTrackCountbackCount);
        }

        public void NewLapUpdate(int lap, PluginManager manager, GameData data)
        {
            if (this.Lap == lap)
                return;


            if (data.GameName != GameName || data.NewData.CarModel != CarName || this.Lap > lap)
            {
                Wipe(manager, data);
                return;
            }


            this.Lap = lap;

            Opponent player = data.NewData.OpponentAtPosition(1, true, false, false);

            Opponent next = data.NewData.OpponentAtPosition(0, true, false, false);
            Opponent behind = data.NewData.OpponentAtPosition(2, true, false, false);

            double infrontGap = Double.NaN;
            double behindGap = Double.NaN;

            if (next != null)
                infrontGap = GetGap(player, next, data);

            if (behind != null)
                behindGap = GetGap(player, behind, data);

            double maximumGapChange = data.NewData.BestLapTime.TotalSeconds * 0.2;
            if (maximumGapChange < 15)
                maximumGapChange = 15;

            if ((next != null && next.Name != CarInfrontName) || (next == null && CarInfrontName != null))
            {
                if (Position != data.NewData.Position || next == null || CarInfrontName == null ||
                    (this.InfrontGap != Double.NaN && infrontGap != Double.NaN && Math.Abs(this.InfrontGap - infrontGap) > maximumGapChange))
                {
                    InfrontDelta.Clear();
                    if (next != null)
                        CarInfrontName = next.Name;
                    else
                        CarInfrontName = null;

                    this.InfrontGap = Double.NaN;
                }
            }

            if ((behind != null && behind.Name != CarBehindName) || (behind == null && CarBehindName != null))
            {
                if (Position != data.NewData.Position || behind == null || CarBehindName == null ||
                    (this.BehindGap != Double.NaN && behindGap != Double.NaN && Math.Abs(this.BehindGap - behindGap) > maximumGapChange))
                {
                    BehindDelta.Clear();
                    if (behind != null)
                        CarBehindName = behind.Name;
                    else
                        CarBehindName = null;

                    this.BehindGap = Double.NaN;
                }
            }
            
            Position = data.NewData.Position;

            //Calculations


            if (Double.IsNaN(this.InfrontGap + infrontGap))
                InfrontDelta.Clear();
            else
                InfrontDelta.Add(this.InfrontGap - infrontGap);

            this.InfrontGap = infrontGap;



            if (Double.IsNaN(this.BehindGap + behindGap))
                BehindDelta.Clear();
            else
                BehindDelta.Add(behindGap - this.BehindGap);

            this.BehindGap = behindGap;

            Write(manager);
        }

        private void Write(PluginManager manager)
        {
            //Write Score
            double[] infrontDeltas = InfrontDelta.GetValues();
            double[] behindDeltas = BehindDelta.GetValues();
            for (int i = 0; i < infrontDeltas.Length && i < behindDeltas.Length; i++)
            {
                manager.SetPropertyValue(AdvancedScoring.NEXTPREFIX + AdvancedScoring.GAPMIDFIX + AdvancedScoring.DELTASUFIX + i, Main.GetType(), infrontDeltas[i]);
                manager.SetPropertyValue(AdvancedScoring.BEHINDPREFIX + AdvancedScoring.GAPMIDFIX + AdvancedScoring.DELTASUFIX + i, Main.GetType(), behindDeltas[i]);
            }


            manager.SetPropertyValue(AdvancedScoring.NEXTPREFIX + AdvancedScoring.GAPMIDFIX + AdvancedScoring.DELTA_AVGSUFIX, Main.GetType(), Average(InfrontDelta.GetValuesOnLength()));
            manager.SetPropertyValue(AdvancedScoring.BEHINDPREFIX + AdvancedScoring.GAPMIDFIX + AdvancedScoring.DELTA_AVGSUFIX, Main.GetType(), Average(BehindDelta.GetValuesOnLength()));
        }

        public void Wipe(PluginManager manager, GameData data)
        {
            Lap = 0;
            CarName = data.NewData.CarModel;
            GameName = data.GameName;

            Position = data.NewData.Position;

            Opponent next = data.NewData.OpponentAtPosition(0, true, false, false);
            if (next != null)
                CarInfrontName = next.Name;
            else
                CarInfrontName = null;

            Opponent previous = data.NewData.OpponentAtPosition(2, true, false, false);
            if (previous != null)
                CarBehindName = previous.Name;
            else
                CarBehindName = null;

            InfrontDelta.Clear();
            BehindDelta.Clear();

            InfrontGap = Double.NaN;
            BehindGap = Double.NaN;

            Write(manager);
        }

        public void UpdateTrackerSetting()
        {
            //This function is called by the UI to update property counts and the like

            //Update Countback count
            if (Main.Settings.GapTrackCountbackCount != InfrontDelta.Length)
            {
                if (Main.Settings.GapTrackCountbackCount > InfrontDelta.Length)
                {
                    //Need to add additional propertys
                    for (int i = InfrontDelta.Length; i < Main.Settings.GapTrackCountbackCount; i++)
                    {
                        Main.PluginManager.AddProperty(AdvancedScoring.NEXTPREFIX + AdvancedScoring.GAPMIDFIX + AdvancedScoring.DELTASUFIX + i, Main.GetType(), Double.MinValue.GetType(), "The delta of the gap to the car infront from the start of that lap to it's end");
                        Main.PluginManager.AddProperty(AdvancedScoring.BEHINDPREFIX + AdvancedScoring.GAPMIDFIX + AdvancedScoring.DELTASUFIX + i, Main.GetType(), Double.MinValue.GetType(), "The delta of the gap to the car behind from the start of that lap to it's end");
                    }
                } //There is no way to delete existing properties

                InfrontDelta = new LinkedQueue<double>(Main.Settings.GapTrackCountbackCount);
                BehindDelta = new LinkedQueue<double>(Main.Settings.GapTrackCountbackCount);
            }
        }

        private double Average(double[] arr)
        {
            if (arr.Length == 0)
                return 0;

            double value = 0;
            for (int i = 0; i < arr.Length; i++)
                value = arr[i] + value;

            value = value / arr.Length;

            return value;
        }

        public double GetGap(Opponent player, Opponent opponent, GameData data)
        {
            double gap = 0;

            if (player == null || opponent == null)
                return gap;

            if (Main.DeltaManager.OverallBestLap.Reference != null && Main.DeltaManager.OverallBestLap.Reference.Eval is double && player.TrackPositionPercent.HasValue && opponent.TrackPositionPercent.HasValue)
            {
                Delta<DoubleWrapper> delta = Main.DeltaManager.OverallBestLap.Reference;
                double referenceLapTime = (double)delta.Eval;

                double playerTrackLoc = player.TrackPositionPercent.Value;
                double opTrackLoc = opponent.TrackPositionPercent.Value;

                double playerTime = delta.GetValue(playerTrackLoc).Value;
                double opTime = delta.GetValue(opTrackLoc).Value;

                if ((player.Position < opponent.Position && playerTrackLoc > opTrackLoc) || (player.Position > opponent.Position && playerTrackLoc < opTrackLoc))
                {
                    //Both are on the same lap
                    
                }
                else if (player.Position < opponent.Position && playerTrackLoc < opTrackLoc)
                {
                    //player is ahead, but has started already another lap
                    playerTime = playerTime + referenceLapTime;
                }
                else if (player.Position > opponent.Position && playerTrackLoc > opTrackLoc)
                {
                    //Opponent is ahead, but has started already another lap
                    opTime = opTime + referenceLapTime;
                }

                gap = playerTime - opTime;
            }
            else if(player.IsPlayer) //Old Fallback
            {
                gap = GetOpponentGap(data, opponent);
            }

            return gap;
        }

        public string GetPrintableGap(Opponent player, Opponent opponent, GameData data)
        {
            string gap = "-.---";

            if (opponent == null || player == null)
                return gap;

            if (player.Equals(opponent)
                || (player.Position == opponent.Position && player.Name == opponent.Name && player.CarName == opponent.CarName)) //in case equals does not work
                return gap;



            //Seeing if there is more then a 1 lap difference
            if (player.CurrentLapHighPrecision.HasValue && opponent.CurrentLapHighPrecision.HasValue)
            {
                if (Math.Abs(opponent.CurrentLapHighPrecision.Value - player.CurrentLapHighPrecision.Value) >= 1)
                {
                    //More then 1 lap difference
                    double lapFrac = player.CurrentLapHighPrecision.Value - opponent.CurrentLapHighPrecision.Value;

                    if (lapFrac > 0)
                        lapFrac = lapFrac - 0.05;
                    else
                        lapFrac = lapFrac + 0.05;


                    lapFrac = Math.Round(lapFrac, 1);
                    return lapFrac + " Laps"; //Exiting
                }
            }

            return string.Format("{0:0.000}", GetGap(player, opponent, data));
        }

        public static double GetOpponentGap(GameData data, Opponent op)
        {
            TimeSpan referenceLap = data.NewData.BestLapTime;

            if (referenceLap == null)
                referenceLap = data.NewData.AllTimeBest;


            if (op != null)
            {
                double gap = Double.NaN;
                if (op.GaptoPlayer != null && op.GaptoPlayer.HasValue)
                {
                    gap = op.GaptoPlayer.Value;
                }
                else if (op.RelativeGapToPlayer != null && op.RelativeGapToPlayer.HasValue)
                {
                    gap = op.RelativeGapToPlayer.Value;

                    if (data.NewData.Position > op.Position && gap > 0)
                    {
                        gap = gap - referenceLap.TotalSeconds;
                    }
                    else if (data.NewData.Position < op.Position && gap < 0)
                    {
                        gap = referenceLap.TotalSeconds + gap;
                    }
                }

                return gap;
            }

            return Double.NaN;
        }

        public static string GetActiveGap(GameData data, Opponent player, Opponent op)
        {
            string gap = "-.---";

            if (op == null || player == null)
                return gap;

            if (player.Equals(op)
                || (player.Position == op.Position && player.Name == op.Name && player.CarName == op.CarName)) //in case equals does not work
                return gap;


            double lap = Double.NaN;
            if (player.CurrentLapHighPrecision.HasValue)
                lap = player.CurrentLapHighPrecision.Value;


            if (op != null)
            {
                if (op.CurrentLapHighPrecision.HasValue && Math.Abs(op.CurrentLapHighPrecision.Value - lap) < 1 || !op.CurrentLapHighPrecision.HasValue)
                {
                    gap = string.Format("{0:0.000}", GetOpponentGap(data, op));
                }
                else
                {
                    double lapFrac = lap - op.CurrentLapHighPrecision.Value;

                    if (lapFrac > 0)
                        lapFrac = lapFrac - 0.05;
                    else
                        lapFrac = lapFrac + 0.05;


                    lapFrac = Math.Round(lapFrac, 1);

                    gap = lapFrac + " Laps";
                }
            }

            return gap;
        }
    }
}
